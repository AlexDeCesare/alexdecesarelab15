package edu.westga.cs1302.linkedlists.model;

import edu.westga.cs1302.linkedlists.errormessages.ErrorMessages;

/**
 * The node of the linked list
 * 
 * @author Alex DeCesare
 * @version 21-July-2020
 *
 * @param <T>
 */

public class Node<T> {

	private T theWord;
	private Node<T> next;
	
	/**
	 * The constructor for the value
	 * 
	 * @precondition none
	 * @postcondition theWord = theValue
	 * 
	 * @param theValue the value of the word
	 */
	
	public Node(T theValue) {
		
		this.setTheWord(theValue);
		this.next = null;
	}
	
	/**
	 * The setter for the value
	 * 
	 * @precondition theValueToSet != null
	 * @postcondition theWord = theValueToSet
	 * 
	 * @param theValueToSet the value to set the word to
	 */
	
	public void setTheWord(T theValueToSet) {
		
		if (theValueToSet == null) {
			throw new IllegalArgumentException(ErrorMessages.THE_VALUE_TO_SET_CANNOT_BE_NULL);
		}
		
		this.theWord = theValueToSet;
	}
	
	/**
	 * The getter for the values
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the word
	 */
	
	public T getTheWord() {
		return this.theWord;
	}
	
	/**
	 * The setter for the node
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param next the next node
	 */
	
	public void setNext(Node<T> next) {
		
		this.next = next;
		
	}
	
	/**
	 * The getter for the node
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the node
	 */
		
	public Node<T> getNext() {
		
		return this.next;
		
	}
	
	/**
	 * The to string for the node
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string description of the node
	 */
	
	@Override 
	public String toString() {

		return "" + this.theWord;
		
	}
}

