package edu.westga.cs1302.linkedlists.model;

import edu.westga.cs1302.linkedlists.errormessages.ErrorMessages;

/**
 * The linked list of the application
 * 
 * @author Alex DeCesare
 * @version 21-July-2020
 * 
 * @param <T>
 */

public class LinkedList<T> {
	
	private Node<T> head;
	
	/**
	 * The constructor for the linked list
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public LinkedList() {
		this.head = null;
	}
	
	/**
	 * Adds a node to the linked list
	 * 
	 * @precondition data != null
	 * @postcondition none
	 * 
	 * @param theContent the content of the node to add
	 */
	
	public void add(T theContent) {
		if (theContent == null) {
			throw new IllegalArgumentException(ErrorMessages.THE_DATA_TO_ADD_CANNOT_EQUAL_NULL);
		}
		
		Node<T> newNode = new Node<T>(theContent);
		
		if (this.head == null) {
			this.head = newNode;
		} else {
			newNode.setNext(this.head);
			this.head = newNode;
		}
		
	}
	
	/**
	 * Gets the reverse list
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description of the reverse list
	 */
	
	public String getReverse() {
		
		this.reverseWords();
		
		String description = "";
		
		Node<T> currentNode = this.head;
		
		while (currentNode != null) {
			
			description += currentNode.toString() + System.lineSeparator();
			currentNode = currentNode.getNext();
		}
		
		return description;
		
	}
	
	/**
	 * Returns the string representation of the linked list
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return description the string representation of the linked list
	 */
	
	public String toString() {
		
		String description = "";
		
		Node<T> currentNode = this.head;
		
		while (currentNode != null) {
			description += currentNode.toString() + System.lineSeparator();
			currentNode = currentNode.getNext();
		}
		
		return description;
	}
	
	private void reverseWords() {
		
		Node<T> previous = null;
		Node<T> current = this.head;
		
		while (current != null) {
			
			Node<T> next = current.getNext();
			current.setNext(previous);
			previous = current;
			current = next;
		}
		
		this.head = previous;
	}

}
