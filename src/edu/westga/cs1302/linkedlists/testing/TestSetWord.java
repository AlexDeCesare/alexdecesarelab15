package edu.westga.cs1302.linkedlists.testing;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.linkedlists.model.Node;

class TestSetWord {

	@Test
	public void shouldNotSetNullWord() {
		
		Node<Object> theNode = new Node<Object>("The Node");
		
		assertThrows(IllegalArgumentException.class, () -> {
			theNode.setTheWord(null);
		});
	}

}
