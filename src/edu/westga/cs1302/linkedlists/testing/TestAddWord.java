package edu.westga.cs1302.linkedlists.testing;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.linkedlists.model.Node;

class TestAddWord {

	@Test
	public void shouldNotAddNullNode() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			new Node<Object>(null);
		});
	}

}
