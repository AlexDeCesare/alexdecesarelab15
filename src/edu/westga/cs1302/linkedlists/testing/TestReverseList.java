package edu.westga.cs1302.linkedlists.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.linkedlists.model.LinkedList;
import edu.westga.cs1302.linkedlists.model.Node;

class TestReverseList {

	@Test
	public void shouldMakeAListOfOneNode() {
		
		LinkedList<Node<String>> theLinkedList = new LinkedList<Node<String>>();
		
		Node<String> theNode = new Node<String>("The Node");
		
		theLinkedList.add(theNode);
		
		assertEquals("The Node" + System.lineSeparator(), theLinkedList.getReverse());
	}
	
	@Test
	public void shouldMakeAListOfMultipleNodes() {
		
		LinkedList<Node<String>> theLinkedList = new LinkedList<Node<String>>();
		
		Node<String> theFirstNode = new Node<String>("The First Node");
		Node<String> theSecondNode = new Node<String>("The Second Node");
		Node<String> theThirdNode = new Node<String>("The Third Node");
		
		theLinkedList.add(theFirstNode);
		theLinkedList.add(theSecondNode);
		theLinkedList.add(theThirdNode);
		
		assertEquals("The First Node" + System.lineSeparator() 
			+ "The Second Node" + System.lineSeparator()
			+ "The Third Node" + System.lineSeparator(), theLinkedList.getReverse());
	}

}
