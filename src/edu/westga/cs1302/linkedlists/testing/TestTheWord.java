package edu.westga.cs1302.linkedlists.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.linkedlists.model.Node;

class TestTheWord {

	@Test
	public void shouldAllowWordIntegerWellAboveZero() {
		
		Node<Integer> theWord = new Node<Integer>(500);
		
		assertEquals(500, theWord.getTheWord());
		
	}
	
	@Test
	public void shouldAllowWordIntegerOneAboveZero() {
		
		Node<Integer> theWord = new Node<Integer>(1);
		
		assertEquals(1, theWord.getTheWord());
		
	}
	
	@Test
	public void shouldAllowWordIntegerAtZero() {
		
		Node<Integer> theWord = new Node<Integer>(0);
		
		assertEquals(0, theWord.getTheWord());
		
	}
	
	@Test
	public void shouldAllowWordIntegerOneBelowZero() {
		
		Node<Integer> theWord = new Node<Integer>(-1);
		
		assertEquals(-1, theWord.getTheWord());
		
	}
	
	@Test
	public void shouldAllowWordIntegerWellBelowZero() {
		
		Node<Integer> theWord = new Node<Integer>(-500);
		
		assertEquals(-500, theWord.getTheWord());
		
	}
	
	@Test 
	public void shouldAllowWordAsTypeStringOneLetter() {
		
		Node<String> theWord = new Node<String>("t");
		
		assertEquals("t", theWord.getTheWord());
		
	}

	@Test 
	public void shouldAllowWordAsTypeStringOneWordLowerCase() {
		
		Node<String> theWord = new Node<String>("wow i can hold any datatype");
		
		assertEquals("wow i can hold any datatype", theWord.getTheWord());
		
	}

	@Test 
	public void shouldAllowWordAsTypeStringOneWordMultipleCase() {
		
		Node<String> theWord = new Node<String>("WOW I Can Hold Any Datatype");
		
		assertEquals("WOW I Can Hold Any Datatype", theWord.getTheWord());
		
	}
}
