package edu.westga.cs1302.linkedlists.errormessages;

/**
 * The error messages for the application
 * 
 * @author Alex DeCesare
 * @version 21-July-2020
 */

public class ErrorMessages {

	public static final String THE_NEXT_FIELD_IS_NULL = "The next field cannot be null";
	public static final String THE_VALUE_TO_SET_CANNOT_BE_NULL = "The value to set cannot be null";
	public static final String THE_DATA_TO_ADD_CANNOT_EQUAL_NULL = "The data to add cannot equal null";
	
}
