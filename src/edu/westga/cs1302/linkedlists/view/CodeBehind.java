package edu.westga.cs1302.linkedlists.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * The code behind class of the application
 * 
 * @author Alex DeCesare
 * @version 21-July-2020
 */

public class CodeBehind {
	
	private ViewModel theViewModel;

    @FXML
    private TextField theInputField;

    @FXML
    private Button buttonAddWord;

    @FXML
    private Button buttonGetList;

    @FXML
    private Label labelTheOrderAddedOutput;

    @FXML
    private Label labelTheReverseOutput;
    
    /**
     * The constructor for the view model
     * 
     * @precondition none
     * @postcondition none
     */
    
    public CodeBehind() {
    	this.theViewModel = new ViewModel();
    }
    
    /**
     * Displays the output
     * 
     * @precondition none
     * @postcondition none
     */
    
    @FXML
    public void displayTheOutput() {
    	
    	try {
        	this.labelTheOrderAddedOutput.setText(this.theViewModel.getListInOrderAdded());
        	this.labelTheReverseOutput.setText(this.theViewModel.getReverseList());
    	} catch (IllegalArgumentException theIllegalArgumentException) {
    		Alert theAlert = new Alert(AlertType.ERROR);
    		theAlert.setContentText(theIllegalArgumentException.getMessage());
    		theAlert.showAndWait();
    	}
    	
    }
    
    /**
     * Adds a node
     * 
     * @precondition none
     * @postcondition none
     */
    
    @FXML 
    public void addNode() {

    	try {
    		this.theViewModel.addNode(this.theInputField.getText());
    	} catch (IllegalArgumentException theIllegalArgumentException) {
    		Alert theAlert = new Alert(AlertType.ERROR);
    		theAlert.setContentText(theIllegalArgumentException.getMessage());
    		theAlert.showAndWait();
    	}
    
    }
	
}
