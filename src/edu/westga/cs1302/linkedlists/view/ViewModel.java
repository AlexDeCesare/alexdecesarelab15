package edu.westga.cs1302.linkedlists.view;

import edu.westga.cs1302.linkedlists.model.LinkedList;

/**
 * The view model of the node
 * 
 * @author Alex DeCesare
 * @version 21-July-2020
 */

public class ViewModel {
	
	private LinkedList<String> theList;
	
	/**
	 * The constructor for the view model
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public ViewModel() {
		
		this.theList = new LinkedList<String>();
		
	}
	
	/**
	 * Gets the list of words in the order added
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list in the order of added nodes
	 */
	
	public String getListInOrderAdded() {
		return this.theList.toString();
	}
	
	/**
	 * Gets the list of words in the reverse order
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of words in reverse order
	 */
	
	public String getReverseList() {
		return this.theList.getReverse();
	}
	
	/**
	 * Adds a node to the linked list
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param theContent the content of the node
	 */
	
	public void addNode(String theContent) {
		
		this.theList.add(theContent);

	}

}
